"""
Resolver el N-Puzzle {8,7,5,3,0,1,4,2,6} usando la busqueda en anchura y profundidad.
Mostrar el resultado en pantalla.
"""
import time
import os
from simpleai.search import astar, SearchProblem

b = "cls"

#Busqueda en Profundidad
META = '''1-2-3
4-5-6
7-8-0'''

DATOS = '''8-7-5
3-0-1
4-2-6'''

tiempo = 0.2 # Tiempo en segundos

def lista_a_string(list_):
    return '\n'.join(['-'.join(row) for row in list_])


def string_a_lista(string_):
    return [row.split('-') for row in string_.split('\n')]


def encontrar_ubicacion(rows, elemento_a_encontrar):
    '''Encuentra la direccion de la ficha buscada, devuelve la tupla fila, columna'''
    for ir, row in enumerate(rows):
        for ic, elemento in enumerate(row):
            if elemento == elemento_a_encontrar:
                return ir, ic


#Se guarda la posicion fnal de cada pieza, para no tener que recalcular en cada ciclo
meta_posicion = {}
rows_goal = string_a_lista(META)
for numero in '123456780':
    meta_posicion[numero] = encontrar_ubicacion(rows_goal, numero)


class Puzzle(SearchProblem):
    def actions(self, state):
        '''Retorna una lista de las piezas que podemso mover a un espacio vacio'''
        rows = string_a_lista(state)
        row_e, col_e = encontrar_ubicacion(rows, '0')

        actions = []
        if row_e > 0:
            actions.append(rows[row_e - 1][col_e])
        if row_e < 2:
            actions.append(rows[row_e + 1][col_e])
        if col_e > 0:
            actions.append(rows[row_e][col_e - 1])
        if col_e < 2:
            actions.append(rows[row_e][col_e + 1])

        return actions

    def result(self, state, action):
        '''retorna el estado resultante despues de mover una ficha a un espacio vacio.
           (La accion parameter contiene la pieza a mover)
        '''
        rows = string_a_lista(state)
        row_e, col_e = encontrar_ubicacion(rows, '0')
        row_n, col_n = encontrar_ubicacion(rows, action)

        rows[row_e][col_e], rows[row_n][col_n] = rows[row_n][col_n], rows[row_e][col_e]

        return lista_a_string(rows)

    def is_goal(self, state):
        '''Devuelve true si el estado actual es el estado deseado'''
        return state == META
     
    def heuristic(self, state):
        '''Retorna una estimacion de la distancia al estado deseado
           Usando la distancia manhatan 
        '''
        rows = string_a_lista(state)

        distance = 0

        for numero in '123456780':
            row_n, col_n = encontrar_ubicacion(rows, numero)
            row_n_goal, col_n_goal = meta_posicion[numero]

            distance += abs(row_n - row_n_goal) + abs(col_n - col_n_goal)

        return distance

result = astar(Puzzle(DATOS))

for action, state in result.path():
    os.system(b)
    print ('Mover el numero ', action)
    print (state)
    time.sleep(tiempo)


input("\nENTER para Continuar")
