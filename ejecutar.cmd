@echo off
title Practicas de Inteligencia Artificial

:inicio
cls
echo.
echo NOTA: Es necesario tener Instalado Python antes de correr cada Programa.
echo.
echo 1. Los 4 Caballos en 3x3
echo 2. N-Puzzle
echo 3. Plano Cartesiano 20x20
echo 4. Instalar Librerias Necesarias / NOTA: Solo la primera vez
echo 5. Salir
echo.
set /p seleccion=-: 

if "%seleccion%" == "1" goto practica1
if "%seleccion%" == "2" goto practica2
if "%seleccion%" == "3" goto practica3
if "%seleccion%" == "4" goto instalarLibrerias
if "%seleccion%" == "5" goto salir

:practica1
python ejercicio1.py
goto inicio

:practica2
python ejercicio2.py
goto inicio

:practica3
python ejercicio3.py
goto inicio

:instalarLibrerias
pip install simpleai matplotlib
goto inicio

:salir
cls