"""
Resolver el problema de los 4 caballos en un tablero 3x3 {2,0,2,0,0,0,1,0,1}
y mostrar el resultado en pantalla.

- Los caballos solo se pueden mover en forma de L.
- Solo se puede realizar un movimiento a la vez.
- Un caballo no puede jugar en una casilla con otro caballo.
- Utilizar busqueda en anchura y profundidad.

"""
import time
import os
import random

b = "cls"
turno = 1
inicio = 1 # Tiempo en segundos

# Busqueda en Profundidad
datos = [2,0,2,0,0,0,1,0,1]
meta = [1,0,1,0,0,0,2,0,2]

def tablero(datos):
    linea = "";
    lineas = []
    for i in datos:
        linea += str(i)
        if len(linea) == 3:
            lineas.append(linea)
            linea = ""
    
    for i in lineas: print(i)

# Calcular todos los movimientos.
def juego(datos,turno):
    confirm = False
    posiciones = []
    for i in range(0,len(datos)):
        if turno == 1:
            if datos[i] == 1:
                posiciones.append(i)
        elif turno == 2:
            if datos[i] == 2:
                posiciones.append(i)

    while confirm == False:
        posicion = posiciones[random.randint(0,1)]
        # Posiciones donde puede Mover
        if posicion == 0:
            # Solo puede moverse a las posiciones 5 o 7
            if datos[5] == 0:
                datos[5] = turno
                datos[0] = 0
                confirm = True
            elif datos[7] == 0:
                datos[7] = turno
                datos[0] = 0
                confirm = True
            else:
                confirm = False

        elif posicion == 1:
            # Solo puede moverse a las posiciones 6 o 8
            if datos[6] == 0:
                datos[6] = turno
                datos[1] = 0
                confirm = True
            elif datos[8] == 0:
                datos[8] = turno
                datos[1] = 0
                confirm = True
            else:
                confirm = False

        elif posicion == 2:
            # Solo puede moverse a las posiciones 3 o 7
            if datos[3] == 0:
                datos[3] = turno
                datos[2] = 0
                confirm = True
            elif datos[7] == 0:
                datos[7] = turno
                datos[2] = 0
                confirm = True
            else:
                confirm = False

        elif posicion == 3:
            # Solo puede moverse a las posiciones 2 o 8
            if datos[2] == 0:
                datos[2] = turno
                datos[3] = 0
                confirm = True
            elif datos[8] == 0:
                datos[8] = turno
                datos[3] = 0
                confirm = True
            else:
                confirm = False
            
        elif posicion == 5:
            # Solo puede moverse a las posiciones 0 o 6
            if datos[0] == 0:
                datos[0] = turno
                datos[5] = 0
                confirm = True
            elif datos[6] == 0:
                datos[6] = turno
                datos[5] = 0
                confirm = True
            else:
                confirm = False
            
        elif posicion == 6:
            # Solo puede moverse a las posiciones 1 o 5
            if datos[1] == 0:
                datos[1] = turno
                datos[6] = 0
                confirm = True
            elif datos[5] == 0:
                datos[5] = turno
                datos[6] = 0
                confirm = True
            else:
                confirm = False
        elif posicion == 7:
            # Solo puede moverse a las posiciones 0 o 2
            if datos[0] == 0:
                datos[0] = turno
                datos[7] = 0
                confirm = True
            elif datos[2] == 0:
                datos[2] = turno
                datos[7] = 0
                confirm = True
            else:
                confirm = False
        elif posicion == 8:
            # Solo puede moverse a las posiciones 1 o 3
            if datos[1] == 0:
                datos[1] = turno
                datos[8] = 0
                confirm = True
            elif datos[3] == 0:
                datos[3] = turno
                datos[8] = 0
                confirm = True
            else:
                confirm = False

    return datos

os.system(b)

if turno > 2:
    turno == 1

tablero(datos)
time.sleep(inicio)
while True:
    os.system(b)
    tablero(datos)
    if datos != meta:
        if turno == 1:
            datos = juego(datos,turno)
            turno = 2
        elif turno == 2:
            datos = juego(datos,turno)
            turno = 1
    else:
        break

input("\nENTER para Continuar")