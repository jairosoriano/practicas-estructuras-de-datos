"""
Generar un espacio 20x20 y usando busqueda en anchura y profundidad encuentre la ruta
desde el punto de inicio al destino. Mostrar resultado en pantalla.

    - Todas las posiciones deben representarse por un punto(x,y)
    - El punto de inicio y fin deben generarse de manera aleatoria
    - Si decide dibujar la representacion del espacio en pantalla sera tomado en cuenta.
"""
import os
from matplotlib import pyplot as plt

import random
# Si está en Windows: cls, si está en Linux: clear
b = "cls"

# Generando un número aleatorio del 1 al 20 para cada eje
INICIO = [random.randint(1,20),random.randint(1,20)]
META = [random.randint(1,20),random.randint(1,20)]

os.system(b)

def dfs(inicio,meta):
    xp = inicio[0]; yp = inicio[1]
    xm = meta[0]; ym = meta[1]
    posiciones = [[xp,yp]]
    posicionx = 0; posiciony = 0

    for i in range(1,200):
        if xp != xm and posicionx != xp:
            posiciony = xp
            if xp > 20:
                xp = 1
            else:
                xp += 1
        
        if yp != ym and posiciony != yp:
            posiciony = yp
            if yp > 20:
                yp = 1
            else:
                yp += 1
        
        posiciones.append([xp,yp])

        if xp == xm and yp == ym:
            return posiciones
            break

print("INICIO: ({},{})".format(INICIO[0],INICIO[1]))
print("META: ({},{})".format(META[0],META[1]))

datos = dfs(INICIO,META)
print("DATOS:\n{}".format(datos))
for i in datos:
    plt.plot(i[0],i[1],'ro')

plt.plot(INICIO[0],INICIO[1],'ko')
plt.plot(META[0],META[1],'yo')
plt.xlabel("INICIO:({},{})  META: ({},{})".format(INICIO[0],INICIO[1],META[0],META[1]))
plt.ylabel("Y")
plt.show()


input("\nENTER para Continuar")